package pl.edu.utp.classchat.services;

import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import pl.edu.utp.classchat.controllers.rest.model.chatroom.AddChatRoomFormRequest;
import pl.edu.utp.classchat.controllers.rest.model.chatroom.ChatRoomBaseInfo;
import pl.edu.utp.classchat.controllers.rest.model.PaginationRequest;
import pl.edu.utp.classchat.controllers.rest.model.chatroom.ChatRoomFullInfo;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by Patryk Bojanczyk
 */
public interface IChatRoomService {

    ResponseEntity<Page<ChatRoomBaseInfo>> getAll(final PaginationRequest paginationRequest);

    ResponseEntity<String> joinToRoom(final String roomId, final HttpServletRequest request);

    ResponseEntity addRoom(final AddChatRoomFormRequest formRequest, final HttpServletRequest request);

    ResponseEntity<ChatRoomFullInfo> getInfo(final String roomId, final HttpServletRequest request);
}
