package pl.edu.utp.classchat.services;

import org.apache.tomcat.util.http.fileupload.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.codec.Base64;
import org.springframework.stereotype.Service;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;

/**
 * Created by Patryk Bojanczyk
 */
@Service
public class FileService implements IFileService {

    private String pathToUpload;

    @Autowired
    public FileService(
            @Value("${file.upload.path}") String pathToUpload
    ) {
        this.pathToUpload = pathToUpload;
    }

    @Override
    public ResponseEntity uploadFile(final MultipartFile file, final HttpServletRequest request) throws IOException {

        File savedFile = new File(pathToUpload+"/"+file.getOriginalFilename());
        file.transferTo(savedFile);

        return ResponseEntity.ok("/rest/file/" + savedFile.getName());
    }

    @Override
    public ResponseEntity getFile(String filename, HttpServletResponse response) throws IOException {

        final File file = new File(pathToUpload + "/" + filename);
        response.getOutputStream().write(FileCopyUtils.copyToByteArray(file));

        if (!file.isFile()) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok().build(); //.body(Base64.encode(FileCopyUtils.copyToByteArray(file)));
    }

}
