package pl.edu.utp.classchat.services;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.Objects;

/**
 * Created by Patryk Bojanczyk
 */
@Service
public class UserService implements IUserService {

    @Override
    public ResponseEntity getUsername(HttpServletRequest request) {
        if (Objects.isNull(request) || Objects.isNull(request.getUserPrincipal())) {
            return ResponseEntity.badRequest().build();
        }

        return ResponseEntity.ok(request.getUserPrincipal().getName());
    }

}
