package pl.edu.utp.classchat.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import pl.edu.utp.classchat.controllers.async.exceptions.ConverterException;
import pl.edu.utp.classchat.controllers.async.model.MessageRequestDTO;
import pl.edu.utp.classchat.controllers.async.model.MessageResponseDTO;
import pl.edu.utp.classchat.controllers.async.model.converters.IMessageConverter;
import pl.edu.utp.classchat.controllers.rest.model.helpers.validators.IValidator;
import pl.edu.utp.classchat.repository.model.ChatRoom;
import pl.edu.utp.classchat.repository.model.Message;
import pl.edu.utp.classchat.repository.model.User;
import pl.edu.utp.classchat.repository.mongo.IChatRoomRepository;
import pl.edu.utp.classchat.repository.mongo.IMessageRepository;
import pl.edu.utp.classchat.repository.mongo.IUserRepository;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;


/**
 * Created by Patryk Bojanczyk
 */
@Service
public class MessageService implements IMessageService {

    private IMessageRepository messageRepository;
    private IMessageConverter messageConverter;
    private IValidator<String> idValidator;
    private final IChatRoomRepository chatRoomRepository;
    private final IUserRepository userRepository;

    @Autowired
    public MessageService(
            final IMessageRepository messageRepository,
            final IMessageConverter messageConverter,
            @Qualifier("idValidator") final IValidator<String> idValidator,
            final IChatRoomRepository chatRoomRepository,
            final IUserRepository userRepository
    ) {
        this.messageRepository = messageRepository;
        this.messageConverter = messageConverter;
        this.idValidator = idValidator;
        this.chatRoomRepository = chatRoomRepository;
        this.userRepository = userRepository;
    }

    @Override
    public MessageResponseDTO save(final MessageRequestDTO message) throws ConverterException {
        final Message entity = messageRepository.save(messageConverter.toMessage(message));

        return messageConverter.toMessageResponseDTO(entity);
    }

    @Override
    public ResponseEntity<List<MessageResponseDTO>> getMessagesByRoom(String roomId, HttpServletRequest request) {
        if (!idValidator.validate(roomId) || request.getUserPrincipal() == null) {
            return ResponseEntity.badRequest().build();
        }

        final ChatRoom chatRoom = chatRoomRepository.findById(roomId);
        final User user = userRepository.findByUsername(request.getUserPrincipal().getName());

        if (Objects.isNull(chatRoom) || Objects.isNull(user) || !chatRoom.getUsers().contains(user.getId())) {
            return ResponseEntity.badRequest().build();
        }

        final PageRequest pageInfo = new PageRequest(0, 30, new Sort(Sort.Direction.ASC, "createTime"));

        Page<Message> list = messageRepository.findByRoomId(roomId, pageInfo);

        return new ResponseEntity<>(
                list.getContent().stream()
                        .map(message -> {
                            try {
                                return messageConverter.toMessageResponseDTO(message);
                            } catch (ConverterException e) {
                                return null;
                            }
                        })
                        .collect(Collectors.toList()),
                HttpStatus.OK
        );
    }

}
