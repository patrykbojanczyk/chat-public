package pl.edu.utp.classchat.services;

import org.springframework.http.ResponseEntity;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by Patryk Bojanczyk
 */
public interface IUserService {

    ResponseEntity getUsername(final HttpServletRequest request);

}
