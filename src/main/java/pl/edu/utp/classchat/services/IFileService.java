package pl.edu.utp.classchat.services;

import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * Created by Patryk Bojanczyk
 */
public interface IFileService {

    ResponseEntity uploadFile(final MultipartFile file, final HttpServletRequest request) throws IOException;

    ResponseEntity getFile(final String filename, final HttpServletResponse response) throws IOException;

}
