package pl.edu.utp.classchat.services;

import org.springframework.http.ResponseEntity;
import pl.edu.utp.classchat.controllers.async.exceptions.ConverterException;
import pl.edu.utp.classchat.controllers.async.model.MessageRequestDTO;
import pl.edu.utp.classchat.controllers.async.model.MessageResponseDTO;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Created by Patryk Bojanczyk
 */
public interface IMessageService {

    MessageResponseDTO save(final MessageRequestDTO message) throws ConverterException;

    ResponseEntity<List<MessageResponseDTO>> getMessagesByRoom(final String roomId, final HttpServletRequest request);
}
