package pl.edu.utp.classchat.services;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import pl.edu.utp.classchat.controllers.rest.model.chatroom.AddChatRoomFormRequest;
import pl.edu.utp.classchat.controllers.rest.model.chatroom.ChatRoomBaseInfo;
import pl.edu.utp.classchat.controllers.rest.model.chatroom.ChatRoomFullInfo;
import pl.edu.utp.classchat.controllers.rest.model.PaginationRequest;
import pl.edu.utp.classchat.controllers.rest.model.helpers.validators.IValidator;
import pl.edu.utp.classchat.repository.model.ChatRoom;
import pl.edu.utp.classchat.repository.model.User;
import pl.edu.utp.classchat.repository.mongo.IChatRoomRepository;
import pl.edu.utp.classchat.repository.mongo.IUserRepository;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * Created by Patryk Bojanczyk
 */
@Service
public class ChatRoomService implements IChatRoomService {

    private final String PROPERTY_TO_SORT = "name";

    private final IChatRoomRepository chatRoomRepository;
    private final IUserRepository userRepository;
    private final Converter<ChatRoom, ChatRoomBaseInfo> chatRoomBaseInfoConverter;
    private final Converter<ChatRoom, ChatRoomFullInfo> chatRoomFullInfoConverter;
    private final Converter<AddChatRoomFormRequest, ChatRoom> addChatRoomFormRequestConverter;
    private final IValidator<PaginationRequest> paginationRequestValidator;
    private final IValidator<String> idValidator;

    @Autowired
    public ChatRoomService(
        final IChatRoomRepository charRoomRepository,
        final IUserRepository userRepository,
        final Converter<ChatRoom, ChatRoomBaseInfo> chatRoomBaseInfoConverter,
        final Converter<ChatRoom, ChatRoomFullInfo> chatRoomFullInfoConverter,
        final Converter<AddChatRoomFormRequest, ChatRoom> addChatRoomFormRequestConverter,
        final IValidator<PaginationRequest> paginationRequestValidator,
        @Qualifier("idValidator") final IValidator<String> idValidator
    ) {
        this.chatRoomRepository = charRoomRepository;
        this.userRepository = userRepository;
        this.chatRoomBaseInfoConverter = chatRoomBaseInfoConverter;
        this.chatRoomFullInfoConverter = chatRoomFullInfoConverter;
        this.addChatRoomFormRequestConverter = addChatRoomFormRequestConverter;
        this.paginationRequestValidator = paginationRequestValidator;
        this.idValidator = idValidator;
    }

    @Override
    public ResponseEntity<Page<ChatRoomBaseInfo>> getAll(
            final PaginationRequest paginationRequest
    ) {
        Page<ChatRoom> chatRoomPage = null;
        final Sort sort = new Sort(new Sort.Order(Sort.Direction.ASC, PROPERTY_TO_SORT));
        Pageable pagination = null;

        if (paginationRequestValidator.validate(paginationRequest)) {
            pagination = new PageRequest(paginationRequest.getPage(), paginationRequest.getSize(), sort);
            chatRoomPage = chatRoomRepository.findAll(pagination);
        } else {
            final List<ChatRoom> chatRooms = chatRoomRepository.findAll(sort);
            chatRoomPage = new PageImpl<>(chatRooms, null, chatRooms.size());
        }

        if (chatRoomPage.getContent().isEmpty()) {
            return ResponseEntity.noContent().build();
        }

        final Page<ChatRoomBaseInfo> chatRoomBaseInfos = new PageImpl<>(
                                chatRoomPage.getContent().stream()
                                    .filter(ChatRoom::getActive)
                                    .map(chatRoomBaseInfoConverter::convert)
                                    .collect(Collectors.toList()),
                                pagination,
                                chatRoomRepository.countByIsActive(true));

        return new ResponseEntity<>(chatRoomBaseInfos, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<String> joinToRoom(
            final String roomId,
            final HttpServletRequest request
    ) {
        if (!idValidator.validate(roomId)) {
            return ResponseEntity.badRequest().build();
        }

        final ChatRoom chatRoom = chatRoomRepository.findById(roomId);
        final User user = userRepository.findByUsername(request.getUserPrincipal().getName());

        if (Objects.isNull(chatRoom) || Objects.isNull(user)) {
            return ResponseEntity.noContent().build();
        }

        if (!chatRoom.getUsers().contains(user.getId())) {
            chatRoom.addUsers(user.getId());
        }

        chatRoomRepository.save(chatRoom);

        return new ResponseEntity<>(roomId, HttpStatus.OK);
    }

    @Override
    public ResponseEntity addRoom(final AddChatRoomFormRequest formRequest, final HttpServletRequest request) {
        if (Objects.isNull(formRequest) || StringUtils.isEmpty(formRequest.getName())) {
            return ResponseEntity.badRequest().build();
        }

        final ChatRoom chatRoom = addChatRoomFormRequestConverter.convert(formRequest);

        chatRoomRepository.save(chatRoom);

        return ResponseEntity.ok().build();
    }

    @Override
    public ResponseEntity<ChatRoomFullInfo> getInfo(
            final String roomId,
            final HttpServletRequest request
    ) {
        if (!idValidator.validate(roomId) || request.getUserPrincipal() == null) {
            return ResponseEntity.badRequest().build();
        }

        final ChatRoom chatRoom = chatRoomRepository.findById(roomId);
        final User user = userRepository.findByUsername(request.getUserPrincipal().getName());

        if (Objects.isNull(chatRoom) || Objects.isNull(user) || !chatRoom.getUsers().contains(user.getId())) {
            return ResponseEntity.badRequest().build();
        }

        return ResponseEntity.ok(chatRoomFullInfoConverter.convert(chatRoom));

    }

}
