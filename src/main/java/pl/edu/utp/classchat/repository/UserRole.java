package pl.edu.utp.classchat.repository;

/**
 * Created by Patryk Bojanczyk
 */
public enum UserRole {
    USER ("USER"),
    TEACHER ("TEACHER"),
    ADMIN ("ADMIN");

    private final String name;

    UserRole(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
