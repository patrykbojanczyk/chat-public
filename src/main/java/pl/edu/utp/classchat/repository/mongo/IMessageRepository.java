package pl.edu.utp.classchat.repository.mongo;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import pl.edu.utp.classchat.repository.model.Message;

/**
 * Created by Patryk Bojanczyk
 */
@Repository
public interface IMessageRepository extends MongoRepository<Message, String> {

    Page<Message> findByRoomId(String roomId, Pageable pageable);
}
