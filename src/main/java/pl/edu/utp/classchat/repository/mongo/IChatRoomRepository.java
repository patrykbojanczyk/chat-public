package pl.edu.utp.classchat.repository.mongo;

import org.springframework.data.mongodb.repository.MongoRepository;
import pl.edu.utp.classchat.repository.model.ChatRoom;

import java.util.UUID;

/**
 * Created by Patryk Bojanczyk
 */
public interface IChatRoomRepository extends MongoRepository<ChatRoom, UUID> {

    Long countByIsActive(Boolean isActive);

    ChatRoom findById(final String roomId);
}
