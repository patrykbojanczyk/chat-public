package pl.edu.utp.classchat.repository.model;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.springframework.data.annotation.Id;

import java.io.Serializable;

/**
 * Created by Patryk Bojanczyk
 */
public class Message implements Serializable {

    @Id
    private String id;
    private final String user;
    private final String roomId;
    private final Long createTime;
    private final String content;

    public Message(String user, String roomId, Long createTime, String content) {
        this.user = user;
        this.roomId = roomId;
        this.createTime = createTime;
        this.content = content;
    }

    public String getId() {
        return id;
    }

    public String getUser() {
        return user;
    }

    public String getRoomId() {
        return roomId;
    }

    public Long getCreateTime() {
        return createTime;
    }

    public String getContent() {
        return content;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        Message message = (Message) o;

        return new EqualsBuilder()
                .append(id, message.id)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(id)
                .toHashCode();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("id", id)
                .append("user", user)
                .append("roomId", roomId)
                .append("createTime", createTime)
                .append("content", content)
                .toString();
    }
}
