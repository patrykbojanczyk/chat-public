package pl.edu.utp.classchat.repository.model;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Created by Patryk Bojanczyk
 */
@Document
public class ChatRoom {

    @Id
    private final String id;
    private final String name;
    private final Boolean isActive;
    private final List<String> users;

    public ChatRoom(String name, Boolean isActive) {
        this.id = UUID.randomUUID().toString();
        this.name = name;
        this.isActive = isActive;
        this.users = new ArrayList<>();
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Boolean getActive() {
        return isActive;
    }

    public List<String> getUsers() {
        return users;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        ChatRoom chatRoom = (ChatRoom) o;

        return new EqualsBuilder()
                .append(id, chatRoom.id)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(id)
                .toHashCode();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("id", id)
                .append("name", name)
                .append("isActive", isActive)
                .append("users", users)
                .toString();
    }

    public void addUsers(final String userId) {
        this.users.add(userId);
    }
}
