package pl.edu.utp.classchat.repository.mongo.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import pl.edu.utp.classchat.repository.model.User;
import pl.edu.utp.classchat.repository.model.security.SecurityUserDetails;
import pl.edu.utp.classchat.repository.mongo.IUserRepository;

/**
 * Created by Patryk Bojanczyk
 */
@Service
public class SecurityUserDetailsService implements ISecurityUserDetailsService {

    @Autowired
    private IUserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        final User user = userRepository.findByUsername(username);
        if(user == null){
            throw new UsernameNotFoundException(username);
        }else{
            return new SecurityUserDetails(user);
        }
    }
}
