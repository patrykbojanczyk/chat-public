package pl.edu.utp.classchat.repository.mongo.security;

import org.springframework.security.core.userdetails.UserDetailsService;

/**
 * Created by Patryk Bojanczyk
 */
public interface ISecurityUserDetailsService extends UserDetailsService {
}
