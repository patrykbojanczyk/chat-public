package pl.edu.utp.classchat.repository.mongo;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import pl.edu.utp.classchat.repository.model.User;

import java.util.UUID;

/**
 * Created by Patryk Bojanczyk
 */
@Repository
public interface IUserRepository extends MongoRepository<User, UUID> {
    User findByUsername(final String username);
    User findById(final String id);
}
