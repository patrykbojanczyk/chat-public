package pl.edu.utp.classchat.config.async;

import org.atmosphere.cpr.MeteorServlet;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.servlet.MultipartConfigElement;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Patryk Bojanczyk
 */
@Configuration
public class AsyncDispatcherServlet {

    private final static long MAX_SIZE_20_MB = 20971520;
    private final static int MAX_SIZE_1_MB = 20971520;

    @Bean
    public MultipartConfigElement multipartConfigElement() {
        return new MultipartConfigElement("", MAX_SIZE_20_MB, MAX_SIZE_1_MB, MAX_SIZE_1_MB);
    }

    @Bean
    public ServletRegistrationBean dispatcherServletRegistration() {
        ServletRegistrationBean registration = new ServletRegistrationBean(new MeteorServlet());
        Map<String,String> params = new HashMap<String,String>();
        params.put("org.atmosphere.servlet","org.springframework.web.servlet.DispatcherServlet");
        params.put("contextClass","org.springframework.web.context.support.AnnotationConfigWebApplicationContext");
        registration.setInitParameters(params);
        registration.setMultipartConfig(multipartConfigElement());

        return registration;
    }

}
