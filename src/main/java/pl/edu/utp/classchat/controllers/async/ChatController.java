package pl.edu.utp.classchat.controllers.async;

import com.google.gson.Gson;
import org.atmosphere.config.service.*;
import org.atmosphere.cpr.AtmosphereResource;
import org.atmosphere.cpr.AtmosphereResourceEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import pl.edu.utp.classchat.controllers.async.exceptions.ConverterException;
import pl.edu.utp.classchat.controllers.async.model.MessageRequestDTO;
import pl.edu.utp.classchat.services.IMessageService;
import pl.edu.utp.classchat.controllers.async.utils.MessageEncoderDecoder;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * Created by Patryk Bojanczyk
 */
@Singleton
@ManagedService(path = "/chat/{room}")
public final class ChatController {
    private static final Logger logger = LoggerFactory.getLogger(ChatController.class);

    private Map<String, List<AtmosphereResource>> rooms;

    private IMessageService messageService;

    @PathParam("room")
    private String room;

    @Autowired
    public ChatController(
            IMessageService messageService
    ) {
        this.rooms = new TreeMap<>();
        this.messageService = messageService;
    }

    @Ready
    public final void onReady(AtmosphereResource atmosphereResource) throws IOException {
        logger.info("Browser {} connected.", atmosphereResource.uuid());
        if (rooms.containsKey(room)) {
            rooms.computeIfAbsent(room, k -> new ArrayList<>());

            rooms.get(room).add(atmosphereResource);
        } else {
            rooms.put(room, Arrays.asList(atmosphereResource));
        }
    }

    @Disconnect
    public final void onDisconnect(final AtmosphereResourceEvent event){
        if(event.isCancelled()) {
            logger.info("Browser {} unexpectedly disconnected", event.getResource().uuid());
        } else if(event.isClosedByClient()) {
            logger.info("Browser {} closed the connection", event.getResource().uuid());
        }
        rooms.remove(room);
    }

    @Message(decoders = {MessageEncoderDecoder.class})
    public final void onMessage(final MessageRequestDTO message) throws IOException, ConverterException {
        logger.info("{} from {} just send {}", message.getUsername(), message.getChatRoomId(), message.getMessage());

        rooms.get(message.getChatRoomId())
                    .forEach(r -> {
                        try {
                            r.getBroadcaster().broadcast(new Gson().toJson(messageService.save(message)));
                        } catch (ConverterException e) {
                            e.printStackTrace();
                        }
                    });
    }
}