package pl.edu.utp.classchat.controllers.rest.model.chatroom;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import pl.edu.utp.classchat.controllers.rest.model.UserDTO;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Patryk Bojanczyk
 */
public class ChatRoomFullInfo {

    private final String id;
    private final String name;
    private final List<UserDTO> users;

    public ChatRoomFullInfo(
            final String id,
            final String name
    ) {
        this.id = id;
        this.name = name;
        this.users = new ArrayList<>();
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public List<UserDTO> getUsers() {
        return users;
    }

    public void addUsers(final List<UserDTO> users) {
        this.users.addAll(users);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        ChatRoomFullInfo that = (ChatRoomFullInfo) o;

        return new EqualsBuilder()
                .append(id, that.id)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(id)
                .toHashCode();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("id", id)
                .append("name", name)
                .append("users", users)
                .toString();
    }
}
