package pl.edu.utp.classchat.controllers.async.exceptions;

/**
 * Created by Patryk Bojanczyk
 */
public class ConverterException extends Exception {

    public ConverterException(String s) {
        super(s);
    }
}
