package pl.edu.utp.classchat.controllers.rest.model.chatroom;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.io.Serializable;

/**
 * Created by Patryk Bojanczyk
 */
public class ChatRoomBaseInfo implements Serializable {

    private final String id;
    private final String name;
    private final Integer size;

    public ChatRoomBaseInfo(final String id, final String name, final Integer size) {
        this.id = id;
        this.name = name;
        this.size = size;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Integer getSize() {
        return size;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        ChatRoomBaseInfo that = (ChatRoomBaseInfo) o;

        return new EqualsBuilder()
                .append(id, that.id)
                .append(name, that.name)
                .append(size, that.size)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(id)
                .append(name)
                .append(size)
                .toHashCode();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("id", id)
                .append("name", name)
                .append("size", size)
                .toString();
    }
}
