package pl.edu.utp.classchat.controllers.rest.model.chatroom;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * Created by Patryk Bojanczyk
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class AddChatRoomFormRequest {
    private String name;
    private boolean isActive;

    @JsonCreator
    public AddChatRoomFormRequest(
            @JsonProperty(value = "name") String name,
            @JsonProperty(value = "isActive") boolean isActive
    ) {
        this.name = name;
        this.isActive = isActive;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        AddChatRoomFormRequest that = (AddChatRoomFormRequest) o;

        return new EqualsBuilder()
                .append(isActive, that.isActive)
                .append(name, that.name)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(name)
                .append(isActive)
                .toHashCode();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("name", name)
                .append("isActive", isActive)
                .toString();
    }
}
