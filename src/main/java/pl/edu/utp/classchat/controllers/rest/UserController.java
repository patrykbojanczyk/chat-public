package pl.edu.utp.classchat.controllers.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.edu.utp.classchat.services.IUserService;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by Patryk Bojanczyk
 */
@RestController
@RequestMapping(value = "/rest/users")
public class UserController {

    private IUserService service;

    @Autowired
    public UserController(
            final IUserService service
    ) {
        this.service = service;
    }


    @GetMapping(
            value = "/get/username"
    )
    public ResponseEntity getUsername(
            final HttpServletRequest request
    ) {
        return service.getUsername(request);
    }

}
