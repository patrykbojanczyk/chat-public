package pl.edu.utp.classchat.controllers.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import pl.edu.utp.classchat.services.IMessageService;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by Patryk Bojanczyk
 */
@RestController
@RequestMapping(value = "/rest/messages")
public class MessageController {

    private final IMessageService service;

    @Autowired
    public MessageController(
            final IMessageService service
    ) {
        this.service = service;
    }

    @GetMapping(
            value = "/get/by/roomId"
    )
    public ResponseEntity getByRoomId(
            @RequestParam(value = "roomId") String roomId,
            final HttpServletRequest request
    ) {
        return service.getMessagesByRoom(roomId, request);
    }
}
