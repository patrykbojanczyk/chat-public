package pl.edu.utp.classchat.controllers.rest.model.helpers.converters;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import pl.edu.utp.classchat.controllers.rest.model.chatroom.AddChatRoomFormRequest;
import pl.edu.utp.classchat.repository.model.ChatRoom;

import java.util.Objects;

/**
 * Created by Patryk Bojanczyk
 */
@Component
public class AddChatRoomFormRequestConverter implements Converter<AddChatRoomFormRequest, ChatRoom> {

    private final Logger logger = LoggerFactory.getLogger(AddChatRoomFormRequestConverter.class);

    @Override
    public ChatRoom convert(AddChatRoomFormRequest source) {
        if ( Objects.isNull(source) ) {
            return null;
        }

        return new ChatRoom(source.getName(), source.isActive());
    }
}
