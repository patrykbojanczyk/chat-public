package pl.edu.utp.classchat.controllers.rest.model.helpers.validators;

import org.apache.commons.lang3.StringUtils;
import org.bson.types.ObjectId;
import org.springframework.stereotype.Component;

import java.util.UUID;

/**
 * Created by Patryk Bojanczyk
 */
@Component
public class IdValidator implements IValidator<String> {

    @Override
    public boolean validate(final String object) {
        if (StringUtils.isEmpty(object)) {
            return false;
        }
        try {
            UUID.fromString(object);
        } catch (final IllegalArgumentException e) {
            return false;
        }

        return true;
    }

}
