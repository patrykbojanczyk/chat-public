package pl.edu.utp.classchat.controllers.async.model.converters;

import pl.edu.utp.classchat.controllers.async.exceptions.ConverterException;
import pl.edu.utp.classchat.controllers.async.model.MessageRequestDTO;
import pl.edu.utp.classchat.controllers.async.model.MessageResponseDTO;
import pl.edu.utp.classchat.repository.model.Message;

/**
 * Created by Patryk Bojanczyk
 */
public interface IMessageConverter {

    Message toMessage(final MessageRequestDTO messageDTO) throws ConverterException;
    MessageResponseDTO toMessageResponseDTO(final Message message) throws ConverterException;

}
