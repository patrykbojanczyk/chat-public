package pl.edu.utp.classchat.controllers.rest.model.helpers.validators;

import org.springframework.stereotype.Component;
import pl.edu.utp.classchat.controllers.rest.model.PaginationRequest;

import java.util.Objects;

/**
 * Created by Patryk Bojanczyk
 */
@Component
public class PaginationRequestValidator implements IValidator<PaginationRequest> {

    @Override
    public boolean validate(PaginationRequest request) {
        return Objects.nonNull(request) && Objects.nonNull(request.getPage()) && Objects.nonNull(request.getSize())
                && request.getPage() >= 0 && request.getSize() > 0;
    }

}
