package pl.edu.utp.classchat.controllers.async.model;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.Date;

/**
 * Created by Patryk Bojanczyk
 */
public class MessageResponseDTO {

    private final String username;
    private final String message;
    private final Long time;

    public MessageResponseDTO() {
        this("", "");
    }

    public MessageResponseDTO(final String username, final String message) {
        this.username = username;
        this.message = message;
        this.time = new Date().getTime();
    }

    public MessageResponseDTO(final String username, final String message, final Long time) {
        this.username = username;
        this.message = message;
        this.time = time;
    }

    public String getUsername() {
        return username;
    }

    public String getMessage() {
        return message;
    }

    public Long getTime() {
        return time;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        MessageResponseDTO that = (MessageResponseDTO) o;

        return new EqualsBuilder()
                .append(username, that.username)
                .append(message, that.message)
                .append(time, that.time)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(username)
                .append(message)
                .append(time)
                .toHashCode();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("username", username)
                .append("message", message)
                .append("time", time)
                .toString();
    }
}
