package pl.edu.utp.classchat.controllers.rest.model.helpers.converters;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import pl.edu.utp.classchat.controllers.rest.model.chatroom.ChatRoomBaseInfo;
import pl.edu.utp.classchat.repository.model.ChatRoom;

/**
 * Created by Patryk Bojanczyk
 */
@Component
public class ChatRoomBaseInfoConverter implements Converter<ChatRoom, ChatRoomBaseInfo> {

    @Override
    public ChatRoomBaseInfo convert(final ChatRoom source) {
        return new ChatRoomBaseInfo(source.getId(), source.getName(), source.getUsers().size());
    }

}
