package pl.edu.utp.classchat.controllers.rest;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by Patryk Bojanczyk
 */
@RestController
@RequestMapping(value = "/rest")
public class MainController {

    @RequestMapping(method = RequestMethod.GET)
    public String index() {
        return "Hello world";
    }

}
