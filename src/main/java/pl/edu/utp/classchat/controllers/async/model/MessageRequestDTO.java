package pl.edu.utp.classchat.controllers.async.model;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.time.LocalDateTime;
import java.util.Date;

/**
 * Created by Patryk Bojanczyk
 */
public class MessageRequestDTO {

    private final String username;
    private final String message;
    private final String chatRoomId;
    private final Long time;

    public MessageRequestDTO() {
        this("", "", "");
    }

    public MessageRequestDTO(String username, String message, String chatRoomId) {
        this.username = username;
        this.message = message;
        this.chatRoomId = chatRoomId;
        this.time = new Date().getTime();
    }

    public String getMessage() {
        return message;
    }

    public String getUsername() {
        return username;
    }

    public String getChatRoomId() {
        return chatRoomId;
    }

    public Long getTime() {
        return time;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        MessageRequestDTO that = (MessageRequestDTO) o;

        return new EqualsBuilder()
                .append(username, that.username)
                .append(message, that.message)
                .append(chatRoomId, that.chatRoomId)
                .append(time, that.time)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(username)
                .append(message)
                .append(chatRoomId)
                .append(time)
                .toHashCode();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("username", username)
                .append("message", message)
                .append("chatRoomId", chatRoomId)
                .append("time", time)
                .toString();
    }
}
