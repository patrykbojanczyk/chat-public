package pl.edu.utp.classchat.controllers;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import pl.edu.utp.classchat.repository.UserRole;
import pl.edu.utp.classchat.repository.model.User;
import pl.edu.utp.classchat.repository.mongo.IUserRepository;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

/**
 * Created by Patryk Bojanczyk
 */
@Controller
@RequestMapping(value = "/login")
public class LoginController {

    @Autowired
    private IUserRepository userRepository;

    @Autowired
    private HttpServletRequest httpServletRequest;

    @RequestMapping(
            method = RequestMethod.GET
    )
    public String login() {
        return "views/login.html";
    }

    @RequestMapping(
            value = "/register",
            method = RequestMethod.POST
    )
    public String register(
            @RequestParam(value = "username", required = false) String username,
            @RequestParam(value = "password", required = false) String password,
            @RequestParam(value = "confirmPassword", required = false) String confirmPassword
    ) {
        if (StringUtils.isNotEmpty(username) &&
                StringUtils.isNotEmpty(password) &&
                StringUtils.isNotEmpty(confirmPassword) &&
                password.equals(confirmPassword)
            ) {
            User user = userRepository.findByUsername(username);
            if (user == null) {
                userRepository.save(new User(username, password, UserRole.USER));
                return "redirect:/login?success";
            }
        }
        return "redirect:/login?register&error";
    }

    @RequestMapping(
            value = "/logout",
            method = RequestMethod.GET
    )
    public String logout() throws ServletException {
        httpServletRequest.logout();

        return "redirect:/login?success";
    }

}
