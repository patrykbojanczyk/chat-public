package pl.edu.utp.classchat.controllers.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import pl.edu.utp.classchat.controllers.rest.model.PaginationRequest;
import pl.edu.utp.classchat.controllers.rest.model.chatroom.AddChatRoomFormRequest;
import pl.edu.utp.classchat.services.IChatRoomService;

import javax.servlet.http.HttpServletRequest;


/**
 * Created by Patryk Bojanczyk
 */
@RestController
@RequestMapping(value = "/rest/rooms")
public class RoomController {

    private final IChatRoomService service;

    @Autowired
    public RoomController(
            final IChatRoomService service
    ) {
        this.service = service;
    }

    @GetMapping(
            value = "/get/all"
    )
    public ResponseEntity getAll(
            PaginationRequest paginationRequest
    ) {
        return service.getAll(paginationRequest);
    }

    @GetMapping(
            value = "/get/info"
    )
    public ResponseEntity checkPrivileges(
            @RequestParam(value = "roomId") String roomId,
            final HttpServletRequest request
    ) {
        return service.getInfo(roomId, request);
    }

    @GetMapping(
            value = "/join"
    )
    public ResponseEntity join(
            @RequestParam(value = "roomId") String roomId,
            final HttpServletRequest request
    ) {
        return service.joinToRoom(roomId, request);
    }

    @PostMapping(
            value = "/add"
    )
    public ResponseEntity add(
            @RequestBody AddChatRoomFormRequest formRequest,
            final HttpServletRequest request
    ) {
        return service.addRoom(formRequest, request);
    }

}
