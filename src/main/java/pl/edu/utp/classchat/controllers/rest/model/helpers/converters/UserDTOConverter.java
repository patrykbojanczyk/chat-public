package pl.edu.utp.classchat.controllers.rest.model.helpers.converters;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import pl.edu.utp.classchat.controllers.rest.model.UserDTO;
import pl.edu.utp.classchat.repository.model.User;

/**
 * Created by Patryk Bojanczyk
 */
@Component
public class UserDTOConverter implements Converter<User, UserDTO> {

    @Override
    public UserDTO convert(final User source) {
        return source == null ? null : new UserDTO(source.getId(), source.getUsername());
    }

}
