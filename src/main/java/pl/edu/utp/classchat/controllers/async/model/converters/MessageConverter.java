package pl.edu.utp.classchat.controllers.async.model.converters;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.edu.utp.classchat.controllers.async.exceptions.ConverterException;
import pl.edu.utp.classchat.controllers.async.model.MessageRequestDTO;
import pl.edu.utp.classchat.controllers.async.model.MessageResponseDTO;
import pl.edu.utp.classchat.repository.model.Message;
import pl.edu.utp.classchat.repository.model.User;
import pl.edu.utp.classchat.repository.mongo.IUserRepository;

import java.util.Objects;
import java.util.UUID;

/**
 * Created by Patryk Bojanczyk
 */
@Component
public class MessageConverter implements IMessageConverter {

    @Autowired
    private IUserRepository userRepository;

    @Override
    public Message toMessage(final MessageRequestDTO messageDTO) throws ConverterException {
        if (Objects.isNull(messageDTO)) {
            throw new ConverterException("Provided MessageResponseDTO, Chat Room or principals is null.");
        }
        final User user = userRepository.findByUsername(messageDTO.getUsername());
        if (Objects.isNull(user)) {
            throw new ConverterException("Cannot find message author.");
        }

        return new Message(user.getUsername(), messageDTO.getChatRoomId(), messageDTO.getTime(), messageDTO.getMessage());
    }

    @Override
    public MessageResponseDTO toMessageResponseDTO(final Message message) throws ConverterException {
        if (Objects.isNull(message)) {
            throw new ConverterException("Provided Message is null.");
        }
        final User user = userRepository.findByUsername(message.getUser());
        if (Objects.isNull(user)) {
            throw new ConverterException("Cannot find message author.");
        }

        return new MessageResponseDTO(user.getUsername(), message.getContent(), message.getCreateTime());
    }

}
