package pl.edu.utp.classchat.controllers.rest.model.helpers.converters;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import pl.edu.utp.classchat.controllers.rest.model.chatroom.ChatRoomFullInfo;
import pl.edu.utp.classchat.controllers.rest.model.UserDTO;
import pl.edu.utp.classchat.repository.model.ChatRoom;
import pl.edu.utp.classchat.repository.model.User;
import pl.edu.utp.classchat.repository.mongo.IUserRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

/**
 * Created by Patryk Bojanczyk
 */
@Component
public class ChatRoomFullInfoConverter implements Converter<ChatRoom, ChatRoomFullInfo> {

    private final Logger logger = LoggerFactory.getLogger(ChatRoomFullInfoConverter.class);

    private final IUserRepository userRepository;
    private final Converter<User, UserDTO> userDTOConverter;

    @Autowired
    public ChatRoomFullInfoConverter(
            final IUserRepository userRepository,
            final Converter<User, UserDTO> userDTOConverter
    ) {
        this.userRepository = userRepository;
        this.userDTOConverter = userDTOConverter;
    }

    @Override
    public ChatRoomFullInfo convert(final ChatRoom source) {
        if ( Objects.isNull(source) ) {
            return null;
        }
        final ChatRoomFullInfo chatRoom = new ChatRoomFullInfo(source.getId(), source.getName());

        chatRoom.addUsers(createUsers(source));

        return chatRoom;
    }

    public List<UserDTO> createUsers(final ChatRoom source) {
        final List<UserDTO> users = new ArrayList<>();
        if (CollectionUtils.isNotEmpty(source.getUsers())) {
            for (String userId : source.getUsers()) {
                try {
                    final User user = userRepository.findById(userId);

                    if (!Objects.isNull(user)) {
                        users.add(userDTOConverter.convert(user));
                    }

                } catch (final IllegalArgumentException e) {
                    logger.debug(e.getMessage());
                }
            }
        }
        return users;
    }

}
