package pl.edu.utp.classchat.controllers.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import pl.edu.utp.classchat.services.IFileService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Patryk Bojanczyk
 */
@RestController
@RequestMapping(value = "/rest/file")
public class FileUploadController {

    private IFileService service;

    @Autowired
    public FileUploadController(
            final IFileService service
    ) {
        this.service = service;
    }

    @PostMapping(
            consumes = MediaType.MULTIPART_FORM_DATA_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity uploadFile(
        @RequestParam(value = "file", required = false) MultipartFile file,
        HttpServletRequest request
    ) throws IOException {
        return service.uploadFile(file, request);
    }

    @GetMapping(
            value = "/{filename:.+}"
    )
    public ResponseEntity getFile(
            @PathVariable(value = "filename") String filename,
            HttpServletResponse response
    ) throws IOException {
        return service.getFile(filename, response);
    }

}
