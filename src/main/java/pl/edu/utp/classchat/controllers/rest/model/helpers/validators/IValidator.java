package pl.edu.utp.classchat.controllers.rest.model.helpers.validators;

/**
 * Created by Patryk Bojanczyk
 */
public interface IValidator<T> {
    boolean validate(T object);
}
