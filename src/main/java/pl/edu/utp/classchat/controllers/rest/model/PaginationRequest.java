package pl.edu.utp.classchat.controllers.rest.model;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * Created by Patryk Bojanczyk
 */
public class PaginationRequest {

    private Integer size;
    private Integer page;

    public PaginationRequest() {
        size = 0;
        page = 0;
    }

    public PaginationRequest(
            final Integer size,
            final Integer page
    ) {
        this.size = size;
        this.page = page;
    }

    public Integer getSize() {
        return size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        PaginationRequest that = (PaginationRequest) o;

        return new EqualsBuilder()
                .append(size, that.size)
                .append(page, that.page)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(size)
                .append(page)
                .toHashCode();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("size", size)
                .append("page", page)
                .toString();
    }
}
