package pl.edu.utp.classchat.controllers.async.utils;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.atmosphere.config.managed.Decoder;
import org.atmosphere.config.managed.Encoder;
import pl.edu.utp.classchat.controllers.async.model.MessageRequestDTO;
import pl.edu.utp.classchat.controllers.async.model.MessageResponseDTO;

import java.io.IOException;

/**
 * Created by Patryk Bojanczyk
 */
public class MessageEncoderDecoder implements Encoder<MessageResponseDTO, String>, Decoder<String, MessageRequestDTO> {

    private final ObjectMapper mapper = new ObjectMapper();

    @Override
    public MessageRequestDTO decode(final String message){
        try{
            return mapper.readValue(message, MessageRequestDTO.class);
        }catch(IOException e){
            throw new RuntimeException(e);
        }
    }

    @Override
    public String encode(MessageResponseDTO message) {
        try{
            return mapper.writeValueAsString(message);
        }catch(IOException e){
            throw new RuntimeException(e);
        }
    }
}
