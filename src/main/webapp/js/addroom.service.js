chatApp.service('AddRoomService', ['$http', function($http) {

    return {
        addRoom: function (room) {
            return $http.post("/rest/rooms/add", { name: room.name, isActive: room.isActive })
                .then(function(response) {
                    if (response.status !== 200) {
                        return null;
                    }
                    return "";
                }, function() {
                    return null;
                });
        }
    }
}]);
