$(document).ready(function() {
    $('#login-form-link').click(function(e) {
        $("#login-form").delay(100).fadeIn(100);
        $("#register-form").fadeOut(100);
        $('#register-form-link').removeClass('active');
        $(this).addClass('active');
        e.preventDefault();
    });
    $('#register-form-link').click(function(e) {
        $("#register-form").delay(100).fadeIn(100);
        $("#login-form").fadeOut(100);
        $('#login-form-link').removeClass('active');
        $(this).addClass('active');
        e.preventDefault();
    });

    var url = window.location.href;
    if (url.split('?')[1] != undefined) {
        var arguments = url.split('?')[1].split('&');
        var index = arguments.indexOf('error');
        if (index >= 0) {
            $('#notification-container-error').show('slow');
        }
        index = arguments.indexOf('success');
        if (index >= 0) {
            $('#notification-container-success').show('slow');
        }
        index = arguments.indexOf('register');
        if (index >= 0) {
            $('#register-form-link').trigger('click');
        }
    }

});