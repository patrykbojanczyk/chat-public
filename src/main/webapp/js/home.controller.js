chatApp.controller('HomeController', ['$scope', 'HomeService', '$location', '$rootScope', function ($scope, $homeService, $location, $rootScope) {

    var defaultPage = 0;
    var defaultSize = 100;

    $scope.chatRoomsAll = {
        content: [],
        first: true,
        last: false,
        number: defaultPage,
        numberOfElements: 0,
        size: defaultSize,
        sort: [],
        totalElements: 0,
        totalPages: 0
    };

    $scope.init = function () {
        $homeService.getAllRooms(defaultPage, defaultSize).then(function (results) {
            if (results.content != undefined && results.content.length > 0) {
                $scope.chatRoomsAll = results;
            }
        });
    };

    $scope.joinToRoom = function (chatRoomId) {
        $homeService.joinToRoom(chatRoomId).then(function (response) {
            if (response == undefined || response == null) {
                $rootScope.showError("You cannot join to room. Maybe try again?");
            } else {
                $rootScope.showSuccess("Okey! Let's chat!");
                $location.path("/room/chat/"+chatRoomId);
            }
        });
    };

    $scope.init();
}]);
