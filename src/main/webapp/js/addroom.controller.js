chatApp.controller('AddRoomController', ['$scope', 'AddRoomService', '$rootScope', '$location', function ($scope, $addRoomService, $rootScope, $location) {

    $scope.room = {
        name: "",
        isActive: false
    };

    $scope.send = function () {
        $addRoomService.addRoom($scope.room)
            .then(function (response) {
                if (response == undefined || response == null) {
                    $rootScope.showError("Cannot add room");
                } else {
                    $rootScope.showSuccess("You add your new room ! Enjoy !");
                    $location.path("#!/");
                }
            });
    }

}]);
