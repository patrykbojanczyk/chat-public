var chatApp = angular.module('angular.atmosphere.chat', ['angular.atmosphere', 'ngRoute', 'ngCookies', 'ngSanitize', 'ui.bootstrap', 'toggle-switch', 'luegg.directives']);

chatApp.config(function($routeProvider, $httpProvider) {
    $routeProvider.when('/', {
        templateUrl : 'views/home.html',
        controller : 'HomeController',
        controllerAs: 'controller'
    })
    .when('/room/chat/:roomId', {
        templateUrl : 'views/chat.html',
        controller : 'ChatController',
        controllerAs: 'controller'
    })
    .when('/room/add', {
        templateUrl : 'views/addroom.html',
        controller : 'AddRoomController',
        controllerAs: 'controller'
    })
    .when('/file', {
        templateUrl : 'views/fileUpload.html',
        controller : 'FileController',
        controllerAs: 'controller'
    }).otherwise('/');

    // $httpProvider.defaults.headers.common["X-Requested-With"] = 'XMLHttpRequest';
});

chatApp.run(function ($location, $rootScope, $interval) {
    $rootScope.$on("$locationChangeStart", function(event, next, current) {
        if(next==current)
            $location.path('/');
    });
    $rootScope.error = false;
    $rootScope.errorMessage = "An error has occurred, please try again.";
    $rootScope.success = false;
    $rootScope.successMessage = "Successfull !";

    $rootScope.showError = function (message) {
        if (message != undefined && message != null && message !== "") {
            $rootScope.errorMessage = message;
        } else {
            $rootScope.errorMessage = "An error has occurred, please try again.";
        }
        $rootScope.error = true;
        $interval($rootScope.hideError, 10000);
    };
    $rootScope.hideError = function () {
        $rootScope.error = false;
        $rootScope.errorMessage = "An error has occurred, please try again.";
    };
    $rootScope.showSuccess = function (message) {
        if (message != undefined && message != null && message !== "") {
            $rootScope.successMessage = message;
        } else {
            $rootScope.successMessage = "Successfull !";
        }
        $rootScope.success = true;
        $interval($rootScope.hideError, 10000);
    };
    $rootScope.hideSuccess = function () {
        $rootScope.success = false;
        $rootScope.successMessage = "Successfull !";
    };
});