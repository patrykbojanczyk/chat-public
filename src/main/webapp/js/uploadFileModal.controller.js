chatApp.controller('UploadFileModalController', ['$scope', '$http', '$uibModalInstance', '$sce', function ($scope, $http, $uibModalInstance, $sce) {

    var DEFAULT_FILE_NAME = "No files have been choose.";

    $scope.fileName = DEFAULT_FILE_NAME;

    $scope.uploadFile = function () {
        var formData = new FormData();
        formData.append("file", file.files[0]);

        console.log(file.files[0]);

        $http.post('/rest/file', formData, {
            headers: {'Content-Type': undefined},
            transformRequest: angular.identity
        }).then(function (response) {
            if (response.status !== 200) {
                return "";
            }
            $scope.sendMessage("Has sent a file. Download at: <a href=\"" + response.data + "\" target=\"_blank\">" + response.data + "</a>");
            $scope.ok();
        });
    };

    $scope.updateName = function (name) {
        if (name == undefined || name === "") {
            $scope.fileName = DEFAULT_FILE_NAME;
        } else {
            $scope.fileName = name;
        }
    };

    $scope.ok = function () {
        $uibModalInstance.close();
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
}]);