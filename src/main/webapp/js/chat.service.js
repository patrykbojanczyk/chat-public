chatApp.service('ChatService', ['$http', function($http) {

    return {
        getRoomInfo: function (roomId) {
            return $http.get("/rest/rooms/get/info", { params: { roomId: roomId }})
                .then(function(response) {
                    if (response.status !== 200) {
                        return {};
                    }
                    return angular.fromJson(response.data);
                }, function() {
                    return [];
                });
        },
        getMessages: function(roomId) {
            return $http.get("/rest/messages/get/by/roomId", { params: { roomId: roomId }})
                .then(function(response) {
                    if (response.status !== 200) {
                        return {};
                    }
                    return angular.fromJson(response.data);
                }, function() {
                    return [];
                });
        },
        getUsername: function () {
            return $http.get("/rest/users/get/username")
                .then(function(response) {
                    if (response.status !== 200) {
                        return null;
                    }
                    return response.data;
                }, function() {
                    return [];
                });
        }
    }
}]);
