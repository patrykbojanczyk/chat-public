chatApp.controller('ChatController',
    ['$scope', 'atmosphereService', '$routeParams', 'ChatService', '$uibModal', '$sce',
    function($scope, atmosphereService, $routeParams, chatService, $uibModal, $sce
) {
    $scope.model = {
        transport: 'websocket',
        messages: []
    };

    $scope.room = {};
    $scope.message = "";

    $scope.socket;

    var request = {
        url: '/chat',
        contentType: 'application/json',
        logLevel: 'debug',
        transport: 'websocket',
        trackMessageLength: true,
        reconnectInterval: 1000000,
        enableXDR: true
    };

    request.onOpen = function(response){
        $scope.model.transport = response.transport;
        $scope.model.connected = true;
        $scope.model.content = 'Atmosphere connected using ' + response.transport;
    };

    request.onClientTimeout = function(response){
        $scope.model.content = 'Client closed the connection after a timeout. Reconnecting in ' + request.reconnectInterval;
        $scope.model.connected = false;
        $scope.socket.push(atmosphere.util.stringifyJSON({ author: author, message: 'is inactive and closed the connection. Will reconnect in ' + request.reconnectInterval }));
        setTimeout(function(){
            $scope.socket = atmosphereService.subscribe(request);
        }, request.reconnectInterval);
    };

    request.onReopen = function(response){
        $scope.model.connected = true;
        $scope.model.content = 'Atmosphere re-connected using ' + response.transport;
    };

    //For demonstration of how you can customize the fallbackTransport using the onTransportFailure function
    request.onTransportFailure = function(errorMsg, request){
        atmosphere.util.info(errorMsg);
        request.fallbackTransport = 'long-polling';
        $scope.model.header = 'Atmosphere Chat. Default transport is WebSocket, fallback is ' + request.fallbackTransport;
    };

    request.onMessage = function(response){
        var responseText = response.responseBody;
        try{
            var message = atmosphere.util.parseJSON(responseText);
            var date = typeof(message.time) === 'string' ? parseInt(message.time) : message.time;
            $scope.model.messages.push({username: message.username, date: new Date(date), message: $sce.trustAsHtml(message.message)});
        }catch(e){
            console.error("Error parsing JSON: ", responseText);
            throw e;
        }
    };

    request.onClose = function(response){
        $scope.model.connected = false;
        $scope.model.content = 'Server closed the connection after a timeout';
    };

    request.onError = function(response){
        $scope.model.content = "Sorry, but there's some problem with your socket or the server is down";
    };

    request.onReconnect = function(request, response){
        $scope.model.content = 'Connection lost. Trying to reconnect ' + request.reconnectInterval;
        $scope.model.connected = false;
    };

    var input = $('#input');
    input.keydown(function(event){
        var message = $(this).val();
        if(message && message.length > 0 && event.keyCode === 13){
            $scope.$apply(function(){
                $scope.sendMessage(message);
            });
        }
    });

    $scope.init = function (request) {
        chatService.getRoomInfo($routeParams.roomId)
            .then(function (result) {
                if (result != undefined) {
                    $scope.room = result;
                }
                console.log($scope.room);
            })
            .then(function() {
                chatService.getUsername()
                    .then(function (result) {
                        if (result != undefined && result != null) {
                            $scope.username = result;
                        }
                    })
                    .then(function () {
                        chatService.getMessages($routeParams.roomId)
                            .then(function (result) {
                                angular.forEach(result, function(value, key) {
                                    var date = typeof(value.time) === 'string' ? parseInt(value.time) : value.time;
                                    $scope.model.messages.push({username: value.username, date: new Date(date), message: value.message});
                                });
                            })
                    })
                    .then(function() {
                        request.url += "/" + $scope.room.id;
                        $scope.socket = atmosphereService.subscribe(request);
                    });
            });
    };

    $scope.init(request);

    $scope.sendMessage = function(message) {
        $scope.socket.push(atmosphere.util.stringifyJSON({username: $scope.username, message: message, chatRoomId: $scope.room.id}));
        $scope.message = '';
    };

    $scope.openFileModal = function() {
        $uibModal.open({
            templateUrl: 'views/modal/fileUpload.html',
            controller: 'UploadFileModalController',
            scope: $scope
        });
    };
}]);
