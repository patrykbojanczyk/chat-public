chatApp.service('HomeService', ['$http', function($http) {

    return {
        getAllRooms: function (page, size) {
            return $http.get("/rest/rooms/get/all", { params: { size: size, page: page }})
                .then(function(response) {
                    if (response.status !== 200) {
                        return [];
                    }
                    return angular.fromJson(response.data);
                }, function() {
                    return [];
                });
        },
        joinToRoom: function (chatRoomId) {
            return $http.get("/rest/rooms/join", { params: { roomId: chatRoomId }})
                .then(function (response) {
                    if (response.status !== 200) {
                        return null;
                    }
                    return "";
                }, function() {
                    return null;
                });
        }
    }
}]);
