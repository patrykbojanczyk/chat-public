var gulp = require('gulp');
var concat = require('gulp-concat');

gulp.task('scripts-js', function() {
    return gulp.src([
            './bower_components/jquery/dist/jquery.min.js',
            './bower_components/angular/angular.min.js',
            './bower_components/angular-bootstrap/ui-bootstrap.min.js',
            './bower_components/angular-bootstrap/ui-bootstrap-tpls.min.js',
            './bower_components/angular-bootstrap-toggle-switch/angular-toggle-switch.min.js',
            './bower_components/angular-route/angular-route.min.js',
            './bower_components/angular-cookies/angular-cookies.min.js',
            './bower_components/angular-scroll-glue/src/scrollglue.js',
            './bower_components/angular-sanitize/angular-sanitize.min.js',
            './bower_components/atmosphere.js/atmosphere.min.js',
            './bower_components/angular-atmosphere/app/scripts/services/angular-atmosphere.js',
            './bower_components/angular-atmosphere-service/service/angular-atmosphere-service.js',
            './js/angular.config.js',
            './js/home.controller.js',
            './js/home.service.js',
            './js/chat.controller.js',
            './js/chat.service.js',
            './js/addroom.controller.js',
            './js/addroom.service.js',
            './js/uploadFileModal.controller.js'
        ])
        .pipe(concat('all.js'))
        .pipe(gulp.dest('./dist/js/'));
});

gulp.task('copy-bs-fonts', function(){
    return gulp.src('./bower_components/bootstrap/fonts/*.{eot,svg,ttf,woff,woff2}')
        .pipe(gulp.dest('./dist/fonts/'));
});

gulp.task('scripts-css', function() {
    return gulp.src([
        './bower_components/bootstrap/dist/css/bootstrap.min.css',
        './bower_components/angular-bootstrap-toggle-switch/style/bootstrap3/angular-toggle-switch-bootstrap-3.css',
        './css/style.css',
        './css/chat.css'
    ])
        .pipe(concat('all.css'))
        .pipe(gulp.dest('./dist/css/'));
});

gulp.task('scripts-js-login', function() {
    return gulp.src([
        './bower_components/jquery/dist/jquery.min.js',
        './bower_components/bootstrap/dist/js/bootstrap.min.js',
        './js/login.js'
    ])
        .pipe(concat('login.js'))
        .pipe(gulp.dest('./dist/js/'));
});

gulp.task('scripts-css-login', function() {
    return gulp.src([
        './bower_components/bootstrap/dist/css/bootstrap.min.css',
        './css/login.css',
        './css/style.css'
    ])
        .pipe(concat('login.css'))
        .pipe(gulp.dest('./dist/css/'));
});